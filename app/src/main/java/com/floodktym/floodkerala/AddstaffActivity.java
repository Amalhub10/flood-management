package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AddstaffActivity extends AppCompatActivity {

    EditText edit_username;
    EditText edit_email;
    EditText edit_pass;
    EditText edit_des;
    EditText edit_phno;
    Button btn_add;
    boolean flag=true;
    private static final String REGISTER_URL="https://floodkerala.000webhostapp.com/php/register.php";
    public String MobilePattern = "[0-9]{10}";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_staff);

        edit_username = (EditText) findViewById(R.id.id_username);
        edit_email = (EditText) findViewById(R.id.id_email);
        edit_des = (EditText) findViewById(R.id.id_des);
        edit_phno = (EditText) findViewById(R.id.id_phno);
        edit_pass = (EditText) findViewById(R.id.id_pass);
        btn_add = (Button) findViewById(R.id.btn_Add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDataEntered();
                if (flag)
                     registerStaff();
            }
      });
    }

    boolean isEmpty(EditText text)
    {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }
    boolean isEmail(EditText text)
    {
        CharSequence edit_email = text.getText().toString();
        return (!TextUtils.isEmpty(edit_email) && Patterns.EMAIL_ADDRESS.matcher((CharSequence) edit_email).matches());
    }


    void checkDataEntered()
    {
        flag=true;
        if (isEmpty(edit_username))
        {
            Toast t = Toast.makeText(this, "You must enter  Name to register!", Toast.LENGTH_SHORT);
            t.show();
            flag=false;
        }
        if (isEmpty(edit_des))
        {
            Toast t = Toast.makeText(this, "You must enter Designation to register!", Toast.LENGTH_SHORT);
            t.show();
            flag=false;
        }
        if (isEmpty(edit_phno))
        {
            Toast t = Toast.makeText(this, "You must enter Phone Number to register!", Toast.LENGTH_SHORT);
            t.show();
            flag=false;
        }
        if (!edit_phno.getText().toString().matches(MobilePattern)) {
            Toast.makeText(getApplicationContext(), "Please enter valid 10 digit phone number", Toast.LENGTH_SHORT).show();
            flag= false;
        }
        if (isEmpty(edit_pass))
        {
            Toast t = Toast.makeText(this, "You must enter Passworg to register!", Toast.LENGTH_SHORT);
            t.show();
            flag=false;
        }
        if (!isEmail(edit_email)) {
            edit_email.setError("Enter valid email!");
            flag=false;
        }
    }
    private void registerStaff() {
        String username = edit_username.getText().toString().trim().toLowerCase();
        String email = edit_email.getText().toString().trim().toLowerCase();
        String password = edit_pass.getText().toString().trim().toLowerCase();
        String designation = edit_des.getText().toString().trim().toLowerCase();
        String phno = edit_phno.getText().toString().trim().toLowerCase();
        register(username, password, email,designation,phno);
    }

    private void register(String username, String password, String email,String designation ,String phno){
        String urlSuffix = "?username=" + username + "&password=" + password + "&email=" + email + "&designation=" + designation +   "&phno=" + phno;
        class RegisterUser extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(AddstaffActivity.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(),"Registered", Toast.LENGTH_SHORT).show();
            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferReader=null;
                try {
                    URL url=new URL(REGISTER_URL+s);
                    HttpURLConnection con=(HttpURLConnection)url.openConnection();
                    bufferReader=new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String result;
                    result=bufferReader.readLine();
                    return  result;

                }catch (Exception e){
                    return null;
                }
            }

        }
        RegisterUser ur=new RegisterUser();
        ur.execute(urlSuffix);
    }

}
