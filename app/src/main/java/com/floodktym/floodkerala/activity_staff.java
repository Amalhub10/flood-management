package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class activity_staff extends AppCompatActivity {

    Button vtm,lgs,btsrh;
    Const constant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);
        lgs = findViewById(R.id.btnlgs);
        lgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        btsrh=findViewById(R.id.btnsrch);
        btsrh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1=new Intent(getApplicationContext(),search.class);
                startActivity(intent1);
            }
        });
        vtm = findViewById(R.id.btnadvictm);
        vtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),viictim.class);
                startActivity(intent);
            }
        });
    }
    private void logout(){
        SharedPreferences sharedPreferences = this.getSharedPreferences(constant.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(constant.LOGGEDIN_SHARED_PREF, false);
        editor.putString(constant.EMAIL_SHARED_PREF, null);
        editor.putString(constant.desig_SHARED_PREF,null);
        editor.commit();
        Intent inten3=new Intent(this,Login.class);
        startActivity(inten3);

    }
}
