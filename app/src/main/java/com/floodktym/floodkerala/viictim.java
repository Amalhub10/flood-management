package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class viictim extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText vname;
    EditText vhname;
    EditText vphno;
    EditText vratiom;
    EditText vaadhar;
    EditText vacno;
    EditText vifsc;
    EditText vpin;
    EditText vcamp;
    Button vctm;
    Spinner spin;
    String vilgname;
    boolean flag=true;
    Const constant;
    JSONObject jsonObject;
    String formattedDate;
    ArrayList villageName=new ArrayList<String>();
    public String AddharPattern = "[0-9]{12}";
    public String MobilePattern = "[0-9]{10}";
    public String RationPattern = "[0-9]{10}";
    public String AnoPattern ="[0-9]";
    public String PinPattern ="[0-9]{6}";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viictim);

        vname = (EditText) findViewById(R.id.vic_name);
        vhname = (EditText) findViewById(R.id.vic_hname);
        vphno = (EditText) findViewById(R.id.vic_phno);
        vratiom = (EditText) findViewById(R.id.vic_ration);
        vaadhar = (EditText) findViewById(R.id.vic_aadhar);
        vacno = (EditText) findViewById(R.id.vic_acno);
        vifsc = (EditText) findViewById(R.id.vic_ifsc);
        vpin = (EditText) findViewById(R.id.vic_pin);
        vcamp = (EditText) findViewById(R.id.vic_camp);
        vctm = (Button) findViewById(R.id.vic_btn);
        spin = (Spinner) findViewById(R.id.vilgsp);
        get_village();
        spin.setOnItemSelectedListener(this);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);

        vctm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDataEntered();
                if (flag)
                registervic();
            }
        });
    }
    boolean isEmpty(EditText text)
    {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }


    void checkDataEntered() {
        flag=true;
        if (isEmpty(vname))
        {
            vname.setError("Enter Name to Register");
            flag=false;
        }

        if (isEmpty(vphno))
        {
            vphno.setError("Enter Phone Number to Regisster");
            flag = false;
            if (!vphno.getText().toString().matches(MobilePattern)) {
                vphno.setError("Please enter Valid 10 digit phone number");
                flag = false;
            }
        }
         if (!isEmpty(vratiom))
        {
            if (!vratiom.getText().toString().matches(RationPattern)) {
                vratiom.setError("Please enter valid 10 digit Ration Card number");
                flag = false;
            }
        }
        if(!isEmpty(vaadhar)) {
            if (!vaadhar.getText().toString().matches(AddharPattern)) {
                vaadhar.setError("Please enter valid 12 digit Aadhar number");
                flag = false;
            }
        }
/*         if(!isEmpty(vacno))
         {
             if (!vacno.getText().toString().matches(AnoPattern))
             {
                 vacno.setError(" Account Number Only Accepted[0-9]");
                 flag = false;
              }
         }*/
         if(!isEmpty(vpin))
         {
             if (!vpin.getText().toString().matches(PinPattern)) {
                 vpin.setError("PIN CODE Only Accepted 6 digits [0-9]");
                 flag = false;
             }
         }

    }

        private void registervic() {
            SharedPreferences sharedPreferences = getSharedPreferences(constant.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            String viname = vname.getText().toString().trim().toLowerCase();
            String vihname = vhname.getText().toString().trim().toLowerCase();
            String viphno = vphno.getText().toString().trim().toLowerCase();
            String viration = vratiom.getText().toString().trim().toLowerCase();
            String viaadhar = vaadhar.getText().toString().trim().toLowerCase();
            String viacno = vacno.getText().toString().trim().toLowerCase();
            String viifsc = vifsc.getText().toString().trim().toLowerCase();
            String vipin = vpin.getText().toString().trim().toLowerCase();
            String vicamp = vcamp.getText().toString().trim().toLowerCase();
            String vistaffid= sharedPreferences.getString(constant.id_SHARED_PREF,null).trim();
            register(viname,vihname,viphno,viration,viaadhar,viifsc,viacno,vipin,vicamp,vistaffid,vilgname,formattedDate);
        }

    private void register(final String viname, final String vihname, final String viphno, final String viration , final String viaadhar, final String viifsc, final String viacno, final String vipin, final String vicamp, final String vistaffid, final String vilgname, final String formattedDate){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,constant.REGISTER_vtm_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            if (response.trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)){
                                Toast.makeText(getApplicationContext(),"Registerd",Toast.LENGTH_LONG).show();

                            }else{
                                Toast.makeText(viictim.this, response+ "Error", Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("vname",viname);
                prams.put("vhname",vihname);
                prams.put("vphno",viphno);
                prams.put("vration",viration);
                prams.put("vaadhar",viaadhar);
                prams.put("vacno",viacno);
                prams.put("vifsc",viifsc);
                prams.put("vpin",vipin);
                prams.put("vcamp",vicamp);
                prams.put("vstaff",vistaffid);
                prams.put("vvilgnam",vilgname);
                prams.put("vdate",formattedDate);
                return prams;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void get_village()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.GET_VILLAGE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("log").trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("taluk_table");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    String name=jo.getString("taluk_name");
                                    villageName.add(name);

                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,villageName);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spin.setAdapter(aa);

                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        vilgname=spin.getItemAtPosition(i).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}


