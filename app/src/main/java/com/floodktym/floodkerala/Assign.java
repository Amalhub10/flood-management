package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Assign extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Const constant;
    JSONObject jsonObject;
    Button asgn;
    Spinner vlgspin,vctmspin,staffspin;
    ArrayList villageName1=new ArrayList<String>();
    ArrayList staffName1=new ArrayList<String>();
    ArrayAdapter aa1;
    String viname2,vlgnam2,vtmname2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign);
        vlgspin = (Spinner) findViewById(R.id.sp_village);
        get_village1();
        vlgspin.setOnItemSelectedListener(this);
        vctmspin=(Spinner)findViewById(R.id.vtm);
        vctmspin.setOnItemSelectedListener(this);
        staffspin=(Spinner)findViewById(R.id.stf);
        get_staff();
        staffspin.setOnItemSelectedListener(this);
        asgn=(Button)findViewById(R.id.btasg);
        asgn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              assgvic();
            }
        });

    }
         private void assgvic()
        {
            viname2 = vctmspin.getSelectedItem().toString().trim().toLowerCase();
            vlgnam2 = vlgspin.getSelectedItem().toString().trim().toLowerCase();
            vtmname2 = staffspin.getSelectedItem().toString().trim().toLowerCase();
            assgn(vlgnam2,vtmname2,viname2);
        }
    private void assgn(final String vlgname, final String vistaffname, final String vtmname3){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,constant.GET_ASG_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),"Registerd",Toast.LENGTH_LONG).show();
                        try {
                            if (response.trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)){


                            }else{
                                Toast.makeText(Assign.this, response+ "Error", Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("vlname",vlgname);
                prams.put("vstafname",vistaffname);
                prams.put("vvtm",vtmname3);
                return prams;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
        private void get_village1()
        {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.GET_VILLAGE_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                jsonObject = new JSONObject(response);
                                if (jsonObject.getString("log").trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("taluk_table");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jo = jsonArray.getJSONObject(i);
                                        String name=jo.getString("taluk_name");
                                        villageName1.add(name);

                                    }
                                }
                                ArrayAdapter aa = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,villageName1);
                                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                vlgspin.setAdapter(aa);



                            }
                            catch (Exception e)
                            {
                                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }){

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    private void get_staff()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.GET_STF_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("log").trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("staff");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    String name=jo.getString("staff_name");
                                    staffName1.add(name);

                                }
                            }
                            ArrayAdapter aa2 = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,staffName1);
                            aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            staffspin.setAdapter(aa2);

                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void get_vctm(final String vilg1)
    {
        final ArrayList vctmName1=new ArrayList<String>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.GET_VTM_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("log").trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("victim_table");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    String name=jo.getString("victim_name");
                                    vctmName1.add(name);

                                }
                            }
                             aa1 = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,vctmName1);
                            aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            vctmspin.setAdapter(aa1);



                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("vname",vilg1);
                return prams;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(adapterView.getId()==R.id.sp_village)
        {
            String vilg1=villageName1.get(i).toString();
            get_vctm(vilg1);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}

