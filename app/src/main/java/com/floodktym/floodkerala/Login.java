package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Login extends AppCompatActivity {
    public static final String LOGIN_URL="https://floodkerala.000webhostapp.com/php/login.php";
    public static final String KEY_EMAIL="email";
    public static final String KEY_PASSWORD="password";
    public static final String LOGIN_SUCCESS="success";
    public static final String SHARED_PREF_NAME="tech";
    public static final String EMAIL_SHARED_PREF="email";
    public static final String LOGGEDIN_SHARED_PREF="loggedin";
    public static final String desig_SHARED_PREF="desig";
    private EditText editTextEmail;
    private EditText editTextPassword;
    private Button BtnLogin;
    private boolean loggedIn=false;
    JSONObject jsonObject;
    Const constant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextEmail=(EditText)findViewById(R.id.editText_email);
        editTextPassword=(EditText)findViewById(R.id.editText_password);
        BtnLogin=(Button)findViewById(R.id.btn_login);
        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {

        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonObject=new JSONObject(response);
                            if(jsonObject.getString("log").trim().equalsIgnoreCase(LOGIN_SUCCESS)){
                                SharedPreferences sharedPreferences = Login.this.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(LOGGEDIN_SHARED_PREF, true);
                                editor.putString(EMAIL_SHARED_PREF, email);
                                editor.putString(desig_SHARED_PREF,jsonObject.getString("desig"));
                                editor.putString(constant.id_SHARED_PREF,jsonObject.getString("id"));
                                editor.commit();

                                if (jsonObject.getString("desig").trim().equals("admin")) {
                                    Intent intent = new Intent(Login.this, AdminActivity.class);
                                    startActivity(intent);
                                }
                                else if (jsonObject.getString("desig").trim().equals("staff"))
                                {
                                    Intent intent = new Intent(Login.this,activity_staff.class);
                                    startActivity(intent);
                                }
                                else if (jsonObject.getString("desig").trim().equals("engineer"))
                                {
                                    Intent intent = new Intent(Login.this,Engineer.class);
                                    startActivity(intent);
                                }
                            }else{
                                Toast.makeText(Login.this, "Invalid username or password", Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put(KEY_EMAIL, email);
                prams.put(KEY_PASSWORD, password);
                return prams;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean(LOGGEDIN_SHARED_PREF, false);
        if(loggedIn){
            Intent intent = new Intent(Login.this, MainActivity.class);
            startActivity(intent);
        }
    }


}
