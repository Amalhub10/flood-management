package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Slabdes extends AppCompatActivity {
    EditText slbde, slbam;
    Boolean flag = true;
    Button btslaab;
    public String AmtPattern ="[0-9]";
    private static final String REGISTER_URL="https://floodkerala.000webhostapp.com/php/slab.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slabdes);
        slbde = (EditText) findViewById(R.id.slabdes);
        slbam = (EditText) findViewById(R.id.slabamt);
        btslaab = (Button) findViewById(R.id.btnslab);
        btslaab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEntered();
                if (flag)
                    Addslb();

            }
        });

    }

    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    void checkDataEntered() {
        flag = true;
        if (isEmpty(slbde)) {
            Toast t = Toast.makeText(this, "Enter Slab Desription!", Toast.LENGTH_SHORT);
            t.show();
            flag = false;
        }
        if (isEmpty(slbam)) {
            Toast t = Toast.makeText(this, "Enter Slab Amount", Toast.LENGTH_SHORT);
            t.show();
            flag = false;
        }
        /*if (!slbam.getText().toString().matches(AmtPattern)) {
            Toast.makeText(getApplicationContext(), "Please enter Number", Toast.LENGTH_SHORT).show();
            flag= false;
        }*/
    }

    private void Addslb() {
        String des = slbde.getText().toString().trim().toLowerCase();
        String amt = slbam.getText().toString().trim().toLowerCase();
        AddSlab(des, amt);
    }
    private void AddSlab( String des,String amt){
        String urlSuffix = "?des=" + des + "&amt=" +amt ;
        class slab extends AsyncTask<String, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(Slabdes.this, "Please Wait", null, true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(),"New Slab Added", Toast.LENGTH_SHORT).show();
            }

            @Override
            protected String doInBackground(String... params) {
                String s = params[0];
                BufferedReader bufferReader=null;
                try {
                    URL url=new URL(REGISTER_URL+s);
                    HttpURLConnection con=(HttpURLConnection)url.openConnection();
                    bufferReader=new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String result;
                    result=bufferReader.readLine();
                    return  result;

                }catch (Exception e){
                    return null;
                }
            }

        }
        slab s1=new slab();
        s1.execute(urlSuffix);
    }
}