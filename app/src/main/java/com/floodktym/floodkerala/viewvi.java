package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class viewvi extends AppCompatActivity {
     EditText ednam,edhnam,edph,edaadh,edrat,edano,edifs;
     Button edbtt;
    JSONObject jsonObject;
    Const constant;
    public String AddharPattern = "[0-9]{12}";
    public String MobilePattern = "[0-9]{10}";
    public String RationPattern = "[0-9]{10}";
    public String AnoPattern ="[0-9]";
    boolean flag=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewvi);
        ednam = (EditText) findViewById(R.id.edname);
        edhnam = (EditText) findViewById(R.id.edhname);
        edph = (EditText) findViewById(R.id.edphno);
        edaadh = (EditText) findViewById(R.id.edaadhar);
        edrat = (EditText) findViewById(R.id.edration);
        edano = (EditText) findViewById(R.id.edacno);
        edifs = (EditText) findViewById(R.id.edifsc);
        Intent intent = getIntent();
        String response = intent.getExtras().getString("response");
        try {
            jsonObject= new JSONObject(response);
            String name=jsonObject.getString("name");
            String hname=jsonObject.getString("hname");
            String phno=jsonObject.getString("phno");
            String aadhar=jsonObject.getString("aadhar");
            String ration=jsonObject.getString("ration");
            String acno=jsonObject.getString("acno");
            String ifsc=jsonObject.getString("ifsc");
            ednam.setText(name);
            edhnam.setText(hname);
            edph.setText(phno);
            edaadh.setText(aadhar);
            edrat.setText(ration);
            edano.setText(acno);
            edifs.setText(ifsc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        edbtt=(Button)findViewById(R.id.edbtn);
        edbtt.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                checkDataEntered();
                edtvic();
            }
        });
    }
    boolean isEmpty(EditText text)
    {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }


    void checkDataEntered() {
        flag=true;

        if (!isEmpty(edrat))
        {
            if (!edrat.getText().toString().matches(RationPattern)) {
                edrat.setError("Please enter valid 10 digit Ration Card number");
                flag = false;
            }
        }
        if(!isEmpty(edaadh)) {
            if (!edaadh.getText().toString().matches(AddharPattern)) {
                edaadh.setError("Please enter valid 12 digit Aadhar number");
                flag = false;
            }
        }
/*         if(!isEmpty(vacno))
         {
             if (!vacno.getText().toString().matches(AnoPattern))
             {
                 vacno.setError(" Account Number Only Accepted[0-9]");
                 flag = false;
              }
         }*/
//        if(!isEmpty(vpin))
//        {
//            if (!vpin.getText().toString().matches(PinPattern)) {
//                vpin.setError("PIN CODE Only Accepted 6 digits [0-9]");
//                flag = false;
//            }
//        }

    }

    private void edtvic() {

        String viname = ednam.getText().toString().trim().toLowerCase();
        String vihname = edhnam.getText().toString().trim().toLowerCase();
        String viphno = edph.getText().toString().trim().toLowerCase();
        String viration = edrat.getText().toString().trim().toLowerCase();
        String viaadhar = edaadh.getText().toString().trim().toLowerCase();
        String viacno = edano.getText().toString().trim().toLowerCase();
        String viifsc = edifs.getText().toString().trim().toLowerCase();
       // String vipin = vpin.getText().toString().trim().toLowerCase();
        edt(viname,vihname,viphno,viration,viaadhar,viifsc,viacno);
    }
    private void edt(final String viname, final String vihname, final String viphno, final String viration , final String viaadhar, final String viifsc, final String viacno){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,constant.REGISTER_edt_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            if (response.trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)){
                                Toast.makeText(getApplicationContext(),"UPDATED",Toast.LENGTH_LONG).show();

                            }else{
                                Toast.makeText(viewvi.this, response+ "Error", Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("vname",viname);
                prams.put("vhname",vihname);
                prams.put("vphno",viphno);
                prams.put("vration",viration);
                prams.put("vaadhar",viaadhar);
                prams.put("vacno",viacno);
                prams.put("vifsc",viifsc);
                return prams;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
