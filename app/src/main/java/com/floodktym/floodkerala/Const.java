package com.floodktym.floodkerala;

public class Const {
    public static final String KEY_EMAIL="email";
    public static final String KEY_PASSWORD="password";
    public static final String LOGIN_SUCCESS="success";
    public static final String SHARED_PREF_NAME="tech";
    public static final String EMAIL_SHARED_PREF="email";
    public static final String LOGGEDIN_SHARED_PREF="loggedin";
    public static final String desig_SHARED_PREF="desig";
    public static final String id_SHARED_PREF="id";
    public static final  String BASE_URL="https://floodkerala.000webhostapp.com/php";
    public static final String REGISTER_vtm_URL=BASE_URL+"/victim.php";
    public static final String REGISTER_srh_URL=BASE_URL+"/srh.php";
    public static final String REGISTER_edt_URL=BASE_URL+"/edt.php";
    public static final String GET_VILLAGE_URL=BASE_URL+"/getvillage.php";
    public static final String GET_VTM_URL=BASE_URL+"/getvictm.php";
    public static final String GET_STF_URL=BASE_URL+"/getstaff.php";
    public static final String GET_ASG_URL=BASE_URL+"/assgn.php";
    public static final String GET_COMP_URL=BASE_URL+"/name.php";
    public static final String GET_SLB_URL=BASE_URL+"/getslab.php";
    public static final String GET_MNy_URL=BASE_URL+"/money.php";
}
