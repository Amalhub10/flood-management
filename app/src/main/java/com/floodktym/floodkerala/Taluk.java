 package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

 public class Taluk extends AppCompatActivity {
    EditText tname;
    boolean flag=true;
    private static final String REGISTER_URL="https://floodkerala.000webhostapp.com/php/taluk.php";
    Button talukadd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taluk);
        tname=(EditText)findViewById(R.id.talukname);
        talukadd=(Button) findViewById(R.id.btntaluk);
        talukadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEntered();
                if (flag)
                    Addtaluk();

            }
        });
    }
    boolean isEmpty(EditText text)
    {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

     void checkDataEntered() {
         flag = true;
         if (isEmpty(tname)) {
             Toast t = Toast.makeText(this, "You must enter  Village Name!", Toast.LENGTH_SHORT);
             t.show();
             flag = false;
         }
     }
     private void Addtaluk() {
         String talukname1 = tname.getText().toString().trim().toLowerCase();
         Addtau(talukname1);
     }
     private void Addtau( String talukname2){
         String urlSuffix = "?talukname2=" +talukname2 ;
         class taaluk extends AsyncTask<String, Void, String> {

             ProgressDialog loading;

             @Override
             protected void onPreExecute() {
                 super.onPreExecute();
                 loading = ProgressDialog.show(Taluk.this, "Please Wait", null, true, true);
             }

             @Override
             protected void onPostExecute(String s) {
                 super.onPostExecute(s);
                 loading.dismiss();
                 Toast.makeText(getApplicationContext(),"New Village Added"+s, Toast.LENGTH_SHORT).show();
             }

             @Override
             protected String doInBackground(String... params) {
                 String s = params[0];
                 BufferedReader bufferReader=null;
                 try {
                     URL url=new URL(REGISTER_URL+s);
                     HttpURLConnection con=(HttpURLConnection)url.openConnection();
                     bufferReader=new BufferedReader(new InputStreamReader(con.getInputStream()));
                     String result;
                     result=bufferReader.readLine();
                     return  result;

                 }catch (Exception e){
                     return null;
                 }
             }

         }
         taaluk t1=new taaluk();
         t1.execute(urlSuffix);
     }

 }
