package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AdminActivity extends AppCompatActivity {

    Button btnAddStaff,btnAddvilg,btnlog,btnslb,btnasg;
    Const constant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        btnAddStaff = findViewById(R.id.btnAddStaff);
        btnAddvilg = findViewById(R.id.btnaddvilg);
        btnasg=findViewById(R.id.btn_assgn);
        btnasg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent6=new Intent(AdminActivity.this,Assign.class);
                startActivity(intent6);
            }
        });
        btnslb=findViewById(R.id.btnslab);
        btnslb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent4=new Intent(AdminActivity.this, Slabdes.class);
                startActivity(intent4);
            }
        });
        btnlog=findViewById(R.id.btnlgt);
        btnlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        btnAddvilg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2=new Intent(AdminActivity.this, Taluk.class);
                startActivity(intent2);
            }
        });
        btnAddStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdminActivity.this, AddstaffActivity.class);
                startActivity(intent);
            }
        });
    }

    private void logout(){
        SharedPreferences sharedPreferences = this.getSharedPreferences(constant.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(constant.LOGGEDIN_SHARED_PREF, false);
        editor.putString(constant.EMAIL_SHARED_PREF, null);
        editor.putString(constant.desig_SHARED_PREF,null);
        editor.commit();
        Intent inten3=new Intent(this,Login.class);
        startActivity(inten3);

    }
}
