package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class search extends AppCompatActivity {

    EditText sname;
    EditText sphn;
    Button sbtn1;
    Const constant;
    JSONObject jsonObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searh);
        sname = (EditText) findViewById(R.id.srnam);
        sphn = (EditText) findViewById(R.id.srphn);
        sbtn1 = (Button) findViewById(R.id.btnsh1);
        sbtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seach();
            }
        });
    }

    private void seach() {
        String srhname = sname.getText().toString().trim().toLowerCase();
        String srhphn = sphn.getText().toString().trim().toLowerCase();
        searchvtm(srhname, srhphn);
    }
    private void searchvtm(final String name, final String phn)
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,constant.REGISTER_srh_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            jsonObject=new JSONObject(response);
                            if(jsonObject.getString("log").trim().equalsIgnoreCase("success"))
                            {
                               Intent i = new Intent(getApplicationContext(), viewvi.class);
                               i.putExtra("response", response);
                               startActivity(i);
                            }else
                                {
                                Toast.makeText(search.this, response+ "Error", Toast.LENGTH_LONG).show();
                                }
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("name1",name);
                prams.put("phno",phn);
                return prams;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    }


