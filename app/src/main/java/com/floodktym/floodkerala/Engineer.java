package com.floodktym.floodkerala;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Engineer extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Button lgt,sa;
    Const constant;
    Spinner vtmname,slab;
    String nam;
    EditText de;
    JSONObject jsonObject;
    ArrayAdapter comp1;
    ArrayList slb1=new ArrayList<String>();
    String name,slbsel;
    String formattedDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_engineer);
        lgt=findViewById(R.id.btnlgt);
        SharedPreferences sharedPreferences = getSharedPreferences(constant.SHARED_PREF_NAME, Context.MODE_PRIVATE);
         nam= sharedPreferences.getString(constant.id_SHARED_PREF,null).trim();
        vtmname=(Spinner)findViewById(R.id.vtmcomp) ;
        getname(nam);
        vtmname.setOnItemSelectedListener(this);
        slab=(Spinner)findViewById(R.id.slb);
        getslb();
        slab.setOnItemSelectedListener(this);
        sa=(Button)findViewById(R.id.btcomp);
        lgt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        sa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add();
            }
        });
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
        de= (EditText) findViewById(R.id.dess);

    }
    private void add()
    {
        name = vtmname.getSelectedItem().toString().trim().toLowerCase();
        slbsel = slab.getSelectedItem().toString().trim().toLowerCase();
        String desr = de.getText().toString().trim().toLowerCase();
        save(name,slbsel,nam,formattedDate,desr);
    }
    private void save(final String name1, final String slabdes1, final String stfnam, final String formattedDate, final String descr){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,constant.GET_MNy_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),"Registerd",Toast.LENGTH_LONG).show();
                        try {
                            if (response.trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)){


                            }else{
                                Toast.makeText(Engineer.this, response+ "Error", Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("name1",name1);
                prams.put("slab11",slabdes1);
                prams.put("vstafname",stfnam);
                prams.put("vdate",formattedDate);
                prams.put("der",descr);
                return prams;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void logout(){
        SharedPreferences sharedPreferences = this.getSharedPreferences(constant.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(constant.LOGGEDIN_SHARED_PREF, false);
        editor.putString(constant.EMAIL_SHARED_PREF, null);
        editor.putString(constant.desig_SHARED_PREF,null);
        editor.commit();
        Intent inten3=new Intent(this,Login.class);
        startActivity(inten3);

    }
    private void getslb()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.GET_SLB_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("log").trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("slab_table");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    String name=jo.getString("slab_des");
                                    slb1.add(name);

                                }
                            }
                            ArrayAdapter slbb = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,slb1);
                            slbb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            slab.setAdapter(slbb);



                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void getname(final String vilg1)
    {
        final ArrayList vctmName1=new ArrayList<String>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, constant.GET_COMP_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            jsonObject = new JSONObject(response);
                            if (jsonObject.getString("log").trim().equalsIgnoreCase(constant.LOGIN_SUCCESS)) {
                                JSONArray jsonArray = jsonObject.getJSONArray("victim_table");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jo = jsonArray.getJSONObject(i);
                                    String name=jo.getString("victim_name");
                                    vctmName1.add(name);

                                }
                            }
                            comp1 = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,vctmName1);
                            comp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            vtmname.setAdapter(comp1);



                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> prams = new HashMap<>();
                prams.put("vname",vilg1);
                return prams;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
